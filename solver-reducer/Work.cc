//
//     MiniRed/GlucoRed
//
//     Siert Wieringa 
//     siert.wieringa@aalto.fi
// (c) Aalto University 2012/2013
//
//
#include"Work.h"
#include"utils/Options.h"

#ifdef MINIRED
using namespace MiniRed;
#elif defined GLUCORED
using namespace GlucoRed;
#endif

static IntOption opt_reducer_work (VERSION_STRING, "work", "Maximum number of clauses in reducer work set", 1000, IntRange(2, INT32_MAX));

Work::Work()
    : free    (0)
    , workset (new E  [(int) opt_reducer_work])
    , space   (new E* [(int) opt_reducer_work])
    , newest  (NULL)
    , oldest  (NULL)
    , best    (NULL)
{
    for( int i = opt_reducer_work-1; i>=0; i-- ) space[i]=&(workset[free++]);
    assert(free==opt_reducer_work);
}

Work::~Work() {
    delete [] space;
    delete [] workset;
}

void Work::updateLocations(ClauseAllocator& ca) {
    E* w = best;
    while ( w ) {
	w->work->updateLocation(ca);
	w = w->worse;
    }
}

WClause* Work::insert(WClause* work, int smetric) {
    WClause* ret = NULL;
    E* w;
    if ( free == 0) {
	w      = oldest;
	ret    = w->work;
	oldest = w->newer;
	if ( w == best   ) best   = w->worse;
	if ( w == newest ) newest = w->older;
	w->remove();
    }
    else w = space[--free];

    w->insert(work, smetric, best, newest);

    newest = w;
    if ( !w->better ) best   = w;
    if ( !w->older  ) oldest = w;

    return ret;
}

WClause* Work::get() {
    assert(available());

    E*       b = best;
    WClause* w = b->work;

    best = b->worse;
    if ( b == oldest ) oldest = b->newer;
    if ( b == newest ) newest = b->older;

    b->remove();
    space[free++] = b;

    return w;
}
