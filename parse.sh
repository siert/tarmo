#!/bin/bash
if [ ! -e "$1" ]; then
    echo "Specify existing testset file"
    exit 1>&2
fi

for V in MiniSAT-CNF MiniSAT-iCNF MiniRed iLingeling-1; do
    echo -n " , "$V" , "
done
echo

for F in $(cat $1); do    
    echo -n $F

    for V in ../minisat-icnf-monitor/log/SAT09/$F/minisat_release-stop-at-sat-52bc92df37be ../minisat-icnf-monitor/log/SAT09-icnf/$F/march_cc-40f23483392e.icnf/minisat_release-stop-at-sat-52bc92df37be log/SAT09-icnf/$F/march_cc-40f23483392e.icnf/minired_release ../minisat-icnf-monitor/log/SAT09-icnf/$F/march_cc-40f23483392e.icnf/ilingeling1; do
	ERR=$V.err
	RES="-"
	if [ ! -e "$ERR" ]; then
	    TIME="nofile"
	elif [ "$(tail $ERR | grep -i limit)" != "" ]; then	    
	    if [ "$(tail $ERR | grep -i limit | grep -i time)" != "" ]; then
		TIME="time_out"
	    elif [ "$(tail $ERR | grep -i limit | grep -i mem)" != "" ]; then
		TIME="mem_out"
	    else
		TIME="limit?"
	    fi
	else
	    TIME=$(./times.sh $ERR | grep CPU | gawk '{ print $2 }')
	    
	    OUT=$(dirname $ERR)/$(basename $ERR err)out
	    RES=$(tail $OUT | gawk '{ if ( $1 == "Bound" ) print $4 }')
	fi
	echo -n " , "$TIME" , "$RES
    done
    echo
done