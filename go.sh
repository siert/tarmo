#!/bin/bash

CNT=0
while [ 1 ]; do
    let CNT=CNT+1
    echo $CNT > test.cnt
    
    ~/cnfuzzdd2009/cnfuzz > cnf

    ./minired cnf -verb=0 > out
    R=$?
    if [ $R == 10 ]; then
	cat out cnf | ./check.sh
	if [ $? != 0 ]; then
	    echo "ERROR: Check failed" 1>&2
	    exit 1
	fi
    fi

    ~/minisat-2.2.0/core/minisat cnf -verb=0 >/dev/null
    M=$?
    if [ $R != $M ]; then
	echo "ERROR: Solvers disagree, result "$R" versus "$M 1>&2
	exit 1
    else
	echo $CNT". solvers agree"
    fi
done