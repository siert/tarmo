#include"MClause.h"
#include"MUSFinder.h"
#include<cstdio>
#define TARJAN_UNDEF -1
int MClause::tarjan_index = 0;
int SCC::scc_ids = 0;
vec<MClause*> MClause::tarjan_stack;

MClause::MClause(const vec<Lit>& lits, int _id) 
    : scc(NULL)
    , id(_id)
    , tarjan_id(TARJAN_UNDEF)
    , tarjan_lowlink(TARJAN_UNDEF) 
{
    lits.copyTo(*this);
}

MClause::~MClause() {
    if ( scc && tarjan_id == tarjan_lowlink ) delete scc;
}

bool MClause::isAssoc (const vec<MClause*>& clauses, vec<lbool> &model, MClause *c) {
    for( int i = 0; i < clauses.size(); i++ ) {
	MClause *d = clauses[i];
	if ( c!=d && 
	     !d->getSCC()->isRedundant() && 
	     !clauses[i]->satisfiedBy(model) ) return false;
    }
    return true;
}


bool MClause::destCheck(MClause *d) {
    if ( MUSFinder::rotation == 3 ) {
	for( int i = 0; i < destLog.size(); i++ ) {
	    if ( destLog[i] == d ) return false;
	}
	destLog.push(d);
    }
    return true;
}

int MClause::setAssoc(vec<lbool> &assoc, const vec<vec<MClause*> >& occ, Lit flip) {
    if ( MUSFinder::rotation == 2 ) {
	if ( srcLog.size() ) {
	    assert(scc->isCritical());
	    for( int i = 0; i < srcLog.size(); i++)
		if ( srcLog[i] == flip ) return 0;
	}
	srcLog.push(flip);
    }
    else if ( MUSFinder::rotation < 2 && 
	      scc->isCritical() ) return 0;

    int ret = scc->setCritical();
    for( int i = 0; i < uniqueEdges.size(); i++ ) {
	MClause *d = uniqueEdges[i].dest;
	assert(d!=this);
	const Lit &l = uniqueEdges[i].lit;
	if ( !d->getSCC()->isRedundant() && l != ~flip && destCheck(d) ) {
	    assert(d->getSCC()->isCritical());
	    assert(assoc[var(l)] != l_Undef );
	    assert(assoc[var(l)] == (sign(l)?l_True:l_False));
	    assoc[var(l)] = sign(l)?l_False:l_True;
	    ret+= d->setAssoc(assoc, occ, l);
	    assoc[var(l)] = sign(l)?l_True:l_False;
	}
    }    

    if ( MUSFinder::rotation > 0 ) {
	for( int i = 0; i < dupEdges.size(); i++ ) {
	    MClause *d = dupEdges[i].dest;
	    const Lit &l = dupEdges[i].lit;
	    if ( !d->scc->isRedundant() && l != ~flip && destCheck(d) ) {
		assert(assoc[var(l)] != l_Undef);
		assert(assoc[var(l)] == (sign(l)?l_True:l_False));
		assoc[var(l)] = sign(l)?l_False:l_True;
		if ( isAssoc(occ[toInt(~l)], assoc, d) )
		    ret+= d->setAssoc(assoc, occ, l);
		assoc[var(l)] = sign(l)?l_True:l_False;	
	    }
	}
    }

    return ret;
}

void MClause::print(FILE *out) const {
    for( int i = 0; i < size(); i++ )
	printf("%s%d ", sign((*this)[i])?"-":"", var((*this)[i])+1);    
    printf("0");
}

bool MClause::satisfiedBy(const vec<lbool> &model) const {
    for( int i = 0; i < size(); i++ ) {
	const Var v = var((*this)[i]);
	assert(model[v] == l_True || model[v] == l_False);
	if ( sign((*this)[i]) == (model[v]==l_False) ) return true;	
    }
    return false;
}

void MClause::addEdge(MClause* dest, Lit lit, bool unique) { 
    Edge e;
    e.dest = dest;
    e.lit  = lit;
    assert(tarjan_id==-1); (unique ? uniqueEdges : dupEdges).push(e);
}

// If there is exactly one literal 'l' in this clause such that '~l' in c then returns var(l), 
// else returns var_Undef.
Var MClause::resolutionVar(MClause &c) {
    if ( c.size() < size() ) return c.resolutionVar(*this);
  
    Var v = var_Undef;
    for( int i = 0; i < size(); i++ ) {
	const Lit &l = ~((*this)[i]);
	for( int j = 0; j < c.size(); j++ ) {
	    if ( c[j] == l ) {
		if ( v != var_Undef ) return var_Undef;
		v = var(l);
		break;
	    }
	}
    }
  
    return v;
}

void MClause::tarjan() {
    assert( tarjan_id == TARJAN_UNDEF );
    assert( scc == NULL );

    tarjan_id      = tarjan_index++;
    tarjan_lowlink = tarjan_id;
    tarjan_stack.push(this);

    for( int i = 0; i < uniqueEdges.size(); i++ ) {
	MClause &c = *(uniqueEdges[i].dest);
	if ( c.tarjan_id == TARJAN_UNDEF ) {
	    c.tarjan();
	    tarjan_lowlink = tarjan_lowlink < c.tarjan_lowlink ? tarjan_lowlink : c.tarjan_lowlink;
	}
	else {
	    for( int j = tarjan_stack.size()-1; j>=0; j-- ) {
		if ( tarjan_stack[j] == &c ) {
		    tarjan_lowlink = tarjan_lowlink < c.tarjan_id ? tarjan_lowlink : c.tarjan_id;
		    break;
		}
	    }
	}
    }

    if ( tarjan_id == tarjan_lowlink ) {
	scc = new SCC();
	MClause *c;
	do {	    
	    c = tarjan_stack.last();
	    assert(c==this || !c->scc);
	    c->scc = scc;
	    scc->push(c);

	    tarjan_stack.pop();
	}
	while( c != this );
    }
}
