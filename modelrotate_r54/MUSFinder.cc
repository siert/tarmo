//
// Written by Siert Wieringa - siert.wieringa@aalto.fi
// (c) Aalto University School of Science 2012
//
//
#include "MUSFinder.h"
#include "mtl/Sort.h"
#include "pipe.h"
#include "solver-reducer/SolRed.h"
#include <cstdio>

#define clSel(i)     (nVars()+i)
#define clFromSel(l) (var(l)-nVars())

IntOption  MUSFinder::verbosity   ("MUSFINDER", "verb",  "Verbosity level (0=silent, 1=normal, 2=verbose).", 1, IntRange(0, 2));
//IntOption  MUSFinder::pickSCC     ("MUSFINDER", "pick-scc", "Which SCC to pick first (0=unsorted, 1=root, 2=smallest root, 3=largest root)\n", 1, IntRange(0,3));
IntOption  MUSFinder::rotation    ("MUSFINDER", "rotation", "Model rotation (0=none, 1=recursive, 2=improved src, 3=improved dest)", 2, IntRange(0,3));
IntOption  MUSFinder::cores       ("MUSFINDER", "cores", "Use Tarmo with this number of cores (0=use minisat)", 0, IntRange(0,100));
BoolOption MUSFinder::reduce      ("MUSFINDER", "reduce", "Use MiniRed instead of Minisat", true);
BoolOption MUSFinder::calcSCC     ("MUSFINDER", "scc", "Compute SCCs.", true);
BoolOption MUSFinder::stats       ("MUSFINDER", "stats", "Output statistics and exit.", false);


MUSFinder::MUSFinder() : varCnt(0), pendingJobs(0) {
    pthread_mutex_init(&lock, NULL);
    pthread_cond_init(&newjobcond, NULL);
}

// Free memory used by clauses
MUSFinder::~MUSFinder() {
    pthread_mutex_destroy(&lock);
    pthread_cond_destroy(&newjobcond);
    for( int i = 0; i < clauses.size(); i++ ) delete clauses[i];
}

// Definition of sorting order for MiniSAT's sorting routine.
bool varLessThan(const Lit x, const Lit y)   { return var(x) < var(y); }

SCC& MUSFinder::chooseSCC() {
    for( int i = 0; i < sccs.size(); i++ ) {
	if ( !sccs[i]->isInSolver() && !sccs[i]->isReachable() ) return *(sccs[i]);
    }

    return *(sccs.last());
}

// Add a clause to the formula in the MUS finder
bool MUSFinder::addClause(vec<Lit>& ps) {
    if ( ps.size() == 0 ) {
        fprintf(stderr, "ERROR: Empty clause appears explicitely in input formula\n");
        exit(1);
    }
    
    // Sort clauses by variable order
    sort(ps, varLessThan);

    // Remove duplicates / prevent addition of tautological clauses
    int i, j;
    for( i=j=1; i < ps.size(); i++ ) {
      if ( ps[i] == ~ps[i-1] )
      return true;
      else if ( ps[i] != ps[i-1] )
      ps[j++] = ps[i];
    }
    ps.shrink(i-j);

    // Add clause pointer to list of clauses
    MClause *c = new MClause(ps, clauses.size());
    clauses.push(c);

    // Add clause pointer to occurence list of each of its literals
    for( i=0; i < ps.size(); i++ ) {
        const int l = toInt(ps[i]);
        while( l >= occ.size() ) occ.push();
        occ[l].push(c);
    }

    return true;
}

void MUSFinder::createEdges(MClause &c, const Lit &l) {
    const vec<MClause*>& ol = occ[toInt(~l)];
    const Var v = var(l);

    // Check if there is exactly one clause 'd' such that 'd' contains '~l' and:
    // for all literals 'k' in 'c' such that 'k' != 'l' it holds that 'd' does not contain '~k'
    MClause *dest=NULL;
    // "not" computing SCCs is done by adding every edge between two clauses as a duplicate edge
    // the result is that every clause will be in an SCC of size 1.
    bool unique = calcSCC; 
    for( int i = 0; i < ol.size(); i++ ) {
        if ( c.resolutionVar(*(ol[i])) == v ) {
	    if ( dest ) {
		unique = false;
		c.addEdge(dest, l, false);
	    }
	    dest = ol[i];
	}
    }

    if ( dest ) c.addEdge(dest, l, unique);
}

void MUSFinder::createEdges(MClause &c) {
    for( int i = 0; i < c.size(); i++ ) {
        const Lit &l = c[i];
	createEdges(c,l);
    }
}

void MUSFinder::printStats() {
    int sccCnt  = 0, sccCntSz1  = 0;
    int rootCnt = 0, rootCntSz1 = 0;
    int l=0;
    int dupOut = 0, uniqOut = 0;

    for( int i = 0; i < clauses.size(); i++ ) {
	const MClause& c = *(clauses[i]);
	const SCC &scc = *(c.getSCC());
	if ( scc[0] == clauses[i] ) {
	    sccCnt++;
	    if ( scc.size()==1 ) sccCntSz1++;

	    if ( !scc.isReachable() ) {
		rootCnt++;
		if ( scc.size()==1 ) rootCntSz1++;
	    }
	}
	l+=c.size();
	dupOut+=c.getDupEdges().size();
	uniqOut+=c.getUniqueEdges().size();
    }

    
    printf("Number of clauses                           : %d\n", clauses.size());
    if ( calcSCC ) {
	printf("Number of SCCs                              : %d (%d of size 1)\n", sccCnt, sccCntSz1);
	printf("Number of root SCCs                         : %d (%d of size 1)\n", rootCnt, rootCntSz1);    
    }
    printf("Average clause length                       : %.2f (%d literals in total)\n", ((float) l) / clauses.size(), l);
    printf("Average rotation edges outdegree            : %.2f (%d edges in E_P \\ E_G)\n", ((float) (uniqOut+dupOut)) / clauses.size(), dupOut);
    if ( calcSCC )
	printf("Average guaranteed rotation edges outdegree : %.2f (%d edges in E_G)\n", ((float) uniqOut) / clauses.size(), uniqOut);    
}

void MUSFinder::handleSAT(MClause *c, vec<lbool>& model) {
    satCnt++;
    assert(!c->getSCC()->isRedundant());

    // Set assignment as assoc for clause '*c'
    int newCrit = c->setAssoc(model, occ);
    if ( newCrit ) {
	effSatCnt++;
	criticalClauses+= newCrit;
	int i, j;
	for( i = j = 0; i < sccs.size(); i++ ) {
	    if ( sccs[i]->isCritical() ) {
		const SCC& scc = *(sccs[i]);
		// "Lock" critical clauses into solver
		for( int j = 0; j < scc.size(); j++ ) {
		    assert(scc[j]->getId()>=0 && scc[j]->getId()<clauses.size());
		    unitClauses.push(mkLit(clSel(scc[j]->getId()), true));
		}
	    }
	    else sccs[j++] = sccs[i];
	}
	assert(i-j>0);
	sccs.shrink(i-j);
    }
}

void MUSFinder::handleUNSAT(vec<Lit>& conflict) {
    unsatCnt++;

    vec<int> conflictSCCs;
    for ( int i = 0; i < conflict.size(); i++ ) {
        const int clId = clFromSel(conflict[i]);	
	const SCC* scc = clauses[clId]->getSCC();
	const int sccId = scc->getId();
	if ( scc->isRedundant() ) return;
	assert(clId==clauses[clId]->getId());
	
	while( sccId >= conflictSCCs.size() ) conflictSCCs.push(0);
	conflictSCCs[sccId]++;
    }

    int i, j;
    for( i = j = 0; i < sccs.size(); i++ ) {
        SCC& scc = *(sccs[i]);
	const int iid = scc.getId();
	// If not all clauses in the SCC occur in the conflict
	// then all clauses in SCC are redundant
	if ( iid >= conflictSCCs.size() || conflictSCCs[iid] < scc.size() ) {
	    scc.setRedundant();
	    clauseCnt-= scc.size();

	    // Permanently deselect redundant clauses in solver
	    for( int j = 0; j < scc.size(); j++ ) {
	        assert(scc[j]->getId()>=0 && scc[j]->getId()<clauses.size());
		unitClauses.push(mkLit(clSel(scc[j]->getId()), false));
	    }
	}
	else sccs[j++] = sccs[i];
    }    
    sccs.shrink(i-j);
    if ( i > j ) effUnsatCnt++;

    // This is needed to recompute which SCCs are roots after some SCCs are removed
    for( int i = 0; i < sccs.size(); i++ )
        sccs[i]->cleanReachableFromList();
}

void MUSFinder::initialize() {
    satCnt = 0;
    unsatCnt = 0;    
    effSatCnt = 0;
    effUnsatCnt = 0;
    unitClauses.clear();
    sccs.clear();
    clauseCnt = clauses.size();
    criticalClauses = 0;

    // Create edges between clauses
    for( int i = 0; i < clauses.size(); i++ ) 
	createEdges(*(clauses[i]));

    // Create SCCs and edges between them
    for( int i = 0; i < clauses.size(); i++ ) {
	MClause& c = *(clauses[i]);
	SCC* scc = c.calcSCC();

	const vec<Edge>& edges = c.getUniqueEdges();
	for( int j = 0; j < edges.size(); j++ ) {
	    SCC *d = edges[j].dest->calcSCC();
	    if ( scc != d ) scc->addEdge(d);
	}
    }
}

void resultFunHelper(void *obj, int jobId, bool result, const int *modelOrConflict) {
    ((MUSFinder*) obj)->handlePipeSolverResult(jobId, result, modelOrConflict);
}

void MUSFinder::handlePipeSolverResult(int jobId, bool result, const int *modelOrConflict) {
    pthread_mutex_lock(&lock);
    

    //printf("job %d finished with result %s\n", jobId, result?"SATISFIABLE":"UNSATISFIABLE"); fflush(stdout);
    //printf("job %d corresponded to clause %d\n", jobId, jobList[jobId]->getId());

    assert(modelOrConflict);

    if ( result ) {
	vec<lbool> model;
	while( *modelOrConflict ) {
	    const int i = *modelOrConflict;
	    const int v = (i < 0) ? -i : i;
	    if ( v <= nVars() ) {
		while( model.size() < v ) model.push(l_Undef);
		model[v-1] = (*modelOrConflict < 0) ? l_False : l_True;
	    }
	    modelOrConflict++;
	}
	handleSAT(jobList[jobId], model);
    }
    else {
	vec<Lit> conflict;
	while( *modelOrConflict ) {
	    const int i = *modelOrConflict;
	    const int v = (i < 0) ? -i : i;
	    conflict.push( mkLit(v-1, i<0) );
	    modelOrConflict++;
	}	
	handleUNSAT(conflict);
    }   

    //printf("finished handling job %d with result %s\n", jobId, result?"SATISFIABLE":"UNSATISFIABLE"); fflush(stdout);

    // stop waiting
    if ( pendingJobs-- == maxJobs )
        pthread_cond_signal(&newjobcond);
    pthread_mutex_unlock(&lock);
}

void MUSFinder::reducePipeSolver(char *const args[]) {
    PipeSolver psolver = PipeSolver(&resultFunHelper, this, args);

    assert(sccs.size() == 0);
    assert(unitClauses.size() == 0);
    jobList.clear();

    // Add clauses extended with selector variables to solver 
    // and create list of SCCs
    for( int i = 0; i < clauses.size(); i++ ) {
	const MClause& c = *(clauses[i]);
	assert(c.getId() == i);
	
	// Copy clause 'c' to 'wc' then add selector variable 'l'
	int wc[c.size()+2];
	for( int j = 0; j < c.size(); j++ ) {
	    const int v = var(c[j])+1;
	    wc[j] = sign(c[j]) ? -v : v;
	}
	wc[c.size()]=1+clSel(c.getId());
	wc[c.size()+1]=0;

	// Add 'wc' to solver
	psolver.addClause(wc);
	    
	// For one "representative" clause in the SCC 
	// push the SCC in the list of SCCs
       	SCC& scc = *c.getSCC();
	if (scc[0]==&c) sccs.push(&scc);
    }
    assert(clauses.size()>1);

    bool workLeft = true;
    while(workLeft) {
	// Lock mutex
	pthread_mutex_lock(&lock);
	if ( pendingJobs >= maxJobs )
	    pthread_cond_wait( &newjobcond, &lock );

	if ( verbosity > 1 )
	    fprintf(stderr, "Clauses: %d / %d\n", clauseCnt, criticalClauses);	

	workLeft = sccs.size()>0;
	if ( workLeft ) {
	    // Add new unit clauses
	    for( int i = 0; i < unitClauses.size(); i++ ) {
		const int v = var(unitClauses[i])+1;
		psolver.addUnitClause( sign(unitClauses[i]) ? -v : v );
	    }
	    unitClauses.clear();	    

	    SCC &r = chooseSCC();
	    assert(!r.isCritical());
	    assert(!r.isRedundant());
	    
	    int assumps[clauseCnt-criticalClauses];
	    int ai = 0;
	    for(int i=0; i<sccs.size(); i++) {
		const SCC& scc = *(sccs[i]);
		assert(!scc.isRedundant() && !scc.isCritical());

		// "Select" each clause of each SCC in the list, except for the clause 'r[0]'
		for( int j = (&scc==&r) ? 1: 0; j < scc.size(); j++ ) {
		    assert(scc[j]->getId()>=0 && scc[j]->getId()<clauses.size());
		    assumps[ai++] = -(clSel(scc[j]->getId())+1);
		}
	    }
	    assert(ai == clauseCnt-criticalClauses-1);
	    assumps[ai] = 0;

	    pendingJobs++;
	    //printf("job: %d will correspond to clause: %d\n", jobList.size(), r[0]->getId());
	    jobList.push(r[0]);
	    r.setInSolver();
	    psolver.addCube(assumps);
	}
	pthread_mutex_unlock(&lock);
    }
}

void MUSFinder::reduceMinisat() {
    MiniRed::SolRed minired;
    Solver  basic;
    Solver& minisat = reduce ? minired: basic;
    while( nVars() + clauses.size() >= minisat.nVars() ) minisat.newVar(); 

    assert(sccs.size() == 0);
    assert(unitClauses.size() == 0);

    // Add clauses extended with selector variables to solver 
    // and create list of SCCs
    for( int i = 0; i < clauses.size(); i++ ) {
	const MClause& c = *(clauses[i]);
	assert(c.getId() == i);
	
	// Copy clause 'c' to 'wc' then add selector variable 'l'
	vec<Lit> wc;
	c.copyTo(wc);
	wc.push(mkLit(clSel(i), false));

	assert(var(wc.last()) <= minisat.nVars());

	// Add 'wc' to solver
	minisat.addClause(wc);
	    
	// For one "representative" clause in the SCC 
	// push the SCC in the list of SCCs
       	SCC& scc = *c.getSCC();
	if (scc[0]==&c) sccs.push(&scc);
    }

    assert(clauses.size()>1);   
    while(sccs.size()) {
	for( int i = 0; i < unitClauses.size(); i++ )
	    minisat.addClause(unitClauses[i]);
	unitClauses.clear();

	if ( verbosity > 1 )
	    fprintf(stderr, "Clauses: %d / %d\n", clauseCnt, criticalClauses);	
	
	const SCC& r = chooseSCC();

	assert(!r.isCritical());
	assert(!r.isRedundant());


	vec<Lit> assumps;
	for(int i=0; i<sccs.size(); i++) {
	    const SCC& scc = *(sccs[i]);

	    // "Select" each clause of each SCC in the list, except for the clause 'r[0]'
	    for( int j = (&scc==&r) ? 1: 0; j < scc.size(); j++ ) {
		assert(scc[j]->getId()>=0 && scc[j]->getId()<clauses.size());
		assumps.push(mkLit(clSel(scc[j]->getId()), true));
	    }
	}

	assert(assumps.size()==clauseCnt-criticalClauses-1);

	if ( minisat.solve(assumps) ) {
	    // SATISFIABLE
	    // Remove selector variables from satisfying assignment
	    minisat.model.shrink(minisat.nVars()-nVars());
	    // Update SCCs
	    handleSAT(r[0], minisat.model);
	    // Result must be that at least SCC r is critical
	    assert( r.isCritical() );
	}
	else {
	    // UNSATISFIABLE
	    // Update SCCs
	    handleUNSAT(minisat.conflict);
	    // Result must be that at least SCC r is redundant
	    assert( r.isRedundant() );
	}	
    }
}

int MUSFinder::go() {
    // Create graphs and SCCs
    initialize();

    // Print statistics and exit if requested to do so
    if ( stats ) {
	printStats();
	return 0;
    }

    if ( cores > 0 ) {
	maxJobs = cores;

	char* solverarg[4];
	solverarg[0] = strdup("./tarmo");
	solverarg[1] = (char*) malloc(10*sizeof(char));
	solverarg[2] = strdup("--input=icnf");
	solverarg[3] = NULL;
	sprintf(solverarg[1], "%d", (int) cores);
	reducePipeSolver(solverarg);

	free(solverarg[0]);
	free(solverarg[1]);
	free(solverarg[2]);
    }   
    else reduceMinisat();

    if ( verbosity > 0 ) {
	fprintf(stderr, "\n#solver calls                   : %d", satCnt+unsatCnt);
	fprintf(stderr, "\n#solver calls with result SAT   : %d", satCnt);
	if ( cores > 1 ) fprintf(stderr, " ( %d effective )", effSatCnt);
	fprintf(stderr, "\n#solver calls with result UNSAT : %d", unsatCnt);
	if ( cores > 1 ) fprintf(stderr, " ( %d effective )", effUnsatCnt);
	fprintf(stderr, "\n#clauses in MUS                 : %d\n\n", clauseCnt);
    }

    printf("p cnf %d %d\n", nVars(), clauseCnt);
    for( int i = 0; i < clauses.size(); i++ ) {
	if ( clauses[i]->getSCC()->isCritical() ) {
	    clauses[i]->print();
	    printf("\n");
	}
    }
    return 0;
}
