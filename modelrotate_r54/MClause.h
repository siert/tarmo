//
// Written by Siert Wieringa - siert.wieringa@aalto.fi
// (c) Aalto University School of Science 2012
//
//
#ifndef mclause_h
#define mclause_h
#include "mtl/Vec.h"
#include "core/SolverTypes.h"
#include "scc.h"
#include <cassert>
#include <cstdio>

using namespace Minisat;

struct Edge {
    MClause *dest;
    Lit lit;
};

class MClause;
class MUSFinder;
class MClause : public vec<Lit> {
public:
    MClause(const vec<Lit>& lits, int _id);  
    ~MClause();

    Var  resolutionVar(MClause &c);
    void addEdge(MClause* dest, Lit lit, bool unique);

    bool isAssoc (const vec<MClause*>& clauses, vec<lbool> &model, MClause *c);
    int  setAssoc(vec<lbool>& a, const vec<vec<MClause*> >& occ, Lit flip = lit_Undef);
    bool destCheck(MClause *d);

    // Get SCC (create if necessary)
    inline SCC* calcSCC ()      { if ( !scc ) tarjan(); return scc; }

    // Get SCC (no side effect)
    inline SCC* getSCC () const { assert(scc); return scc; }

    // Get information (no side effects)
    inline const vec<Edge>& getDupEdges    () const { return dupEdges; }
    inline const vec<Edge>& getUniqueEdges () const { return uniqueEdges; }
    inline const int        getId          () const { return id; }
    bool satisfiedBy(const vec<lbool> &model) const;

    void print(FILE *out = stdout) const;
private:
    void tarjan();

    vec<Edge>     dupEdges;
    vec<Edge>     uniqueEdges;
    vec<Lit>      srcLog;
    vec<MClause*> destLog;
    SCC*          scc;
    const int     id;

    int tarjan_id;
    int tarjan_lowlink;
    static int tarjan_index;
    static vec<MClause*> tarjan_stack;
};

#endif
