// Model rotation & graph manipulation
//
// Written by Siert Wieringa - siert.wieringa@aalto.fi
// (c) Aalto University School of Science 2012
//
//
#include <zlib.h>
#include "core/Dimacs.h"
#include "utils/Options.h"
#include "MUSFinder.h"

int main(int argc, char** argv) {
  // Parse command line parameters
  parseOptions(argc, argv, true);

  // Open file
  gzFile in = (argc == 1) ? gzdopen(0, "rb") : gzopen(argv[1], "rb");
  if (in == NULL)
    fprintf(stderr, "ERROR! Could not open file: %s\n", argc == 1 ? "<stdin>" : argv[1]), exit(1);

  // Create MUS finder
  MUSFinder m;

  // Read file into MUS finder and close file
  parse_DIMACS(in, m);
  gzclose(in);

  // Run MUS finder
  return m.go();
}
