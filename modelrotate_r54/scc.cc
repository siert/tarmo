#include"scc.h"
#include"MClause.h"
#include"mtl/Sort.h"

void SCC::addEdge(SCC *c) { 
    for( int i = 0; i < edges.size(); i++ ) { 
	if ( edges[i] == c ) return; 
    } 
    edges.push(c);

    // This is not very efficient and only necessary to recompute new
    // roots if redundant SCCs are dropped 
    // (which is only necessary if SCC sorting is enabled)
    for( int i = 0; i < c->reachableFrom.size(); i++ ) { 
	if ( c->reachableFrom[i] == this ) return; 
    } 
    c->reachableFrom.push(this);
}

void SCC::cleanReachableFromList() {
    int i, j;
    for ( i = j = 0; i < reachableFrom.size(); i++ )
	if ( !reachableFrom[i]->redundant ) reachableFrom[j++] = reachableFrom[i];
    reachableFrom.shrink(i-j);
}

int SCC::setCritical() { 
    assert(!redundant); 
    if ( critical ) return 0;
    
    critical = true; 
    int ret = size();
    for( int i = 0; i < edges.size(); i++ )
	if ( !edges[i]->isRedundant() )	ret+= edges[i]->setCritical();

    return ret;
}
