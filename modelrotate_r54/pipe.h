#ifndef pipe_h
#define pipe_h
#include<cstdio>
#include<cstdlib>
#include<pthread.h>

/*
  This provides access to an incremental SAT-solver through UNIX pipes

  - A Boolean variable is represented by an integer >0
  - A literal is either a variable (integer), or its negation (negative integer)
  - Clauses, cubes, satisfying truth assignments, and final conflicts are all represented as 
    sequences of literals (integers) terminated by 0

  A call to addCube returns an integer that equals the number of times it has been called
  before, this equals the job id of the job created by adding this cube.

  When creating an instance of this class a pointer must be provided to a function that 
  will handle reporting of results. This function is then called from a separate POSIX
  thread that is created by the constructor. The function must have signature 
  'void fun(int, bool, const int*)'. The first parameter corresponds to the job id
  for which a result is reported. The second parameter contains to the result and 
  should be compared to the constants PipeSolver::SATISFIABLE or PipeSolver::UNSATISFIABLE, 
  the final parameter points to a sequence of integers that is either a final conflict 
  clause (in case of UNSAT result) or a satisfying assignment (in case of SAT result).
 */

// Provides access to any incremental solver through UNIX pipes
class PipeSolver {
public:
    // Constructors
    PipeSolver(void (*fptr)(void*, int, bool, const int*), void *_data, char *const args[]);
    // Destructor
    ~PipeSolver();

    // add a cube (creates a job)
    int addCube(const int *cube);
    int addNegationAsCube(const int *clause);

    // add a clause
    void addClause (const int* clause);
    // add a clause of length 1
    void addUnitClause(const int lit);

    // Constants for reporting result
    static const bool SATISFIABLE, UNSATISFIABLE;
private:
    // Signal handler
    static void signalHandler(int signum);
    // Reads solver's output and reports result to caller
    void readThread();
    // Helper to start the readThread function through pthread_create
    inline static void* readThreadEntry(void *p) { ((PipeSolver*) p)->readThread(); return NULL; }


    // Function pointer for reporting solver results
    void (*reportResult)(void *data, int job, bool result, const int* array);
    // Data to be passed back to the function reportResult
    void *data;
    // Stream from solver to this program
    FILE *in;
    // Stream from this program to solver
    FILE *out;
    // Reading thread
    pthread_t rthread;
    // Number of cubes added
    int numCubes;
    // PID of solver process
    pid_t solverpid;
};

#endif
