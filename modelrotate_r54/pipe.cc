
#include"pipe.h"
#include<cstdio>
#include<cassert>
#include<cstdlib>
#include<unistd.h>
#include<csignal>
#include<cstdarg>
#include<sys/wait.h>

#define READ  0
#define WRITE 1
#define ALLOC 1024

const bool PipeSolver::SATISFIABLE = true;
const bool PipeSolver::UNSATISFIABLE = false; 

// Static helper function, prints a constraint (cube/clause)
static void printConstraint(FILE *fp, const int* c) {
    assert( c );
    while( *c ) fprintf(fp, "%d ", *(c++));
    fprintf(fp, "0\n");
}

// Static helper function, prints error message and exits
static void die(const char* s, ...) {
    va_list vl;
    fprintf(stderr, "ERROR: ");
    va_start(vl, s);
    vfprintf(stderr, s, vl);
    va_end(vl);
    fputc('\n', stderr);
    exit(1);
}

// Constructor
PipeSolver::PipeSolver(void (*fptr)(void*, int, bool, const int*), void *_data, char *const args[]) 
  : reportResult(fptr), data(_data), in(NULL), out(NULL), numCubes(0), solverpid(-1)
{
    int toSolver[2];
    int fromSolver[2];

    assert(args);
    assert(*args);

    // Create pipes, exit if not succesful or pipe assigned to stdout or stderr
    if ( pipe(toSolver)   || 
	 pipe(fromSolver) ||
	 toSolver[READ]    <= 1 || 
	 toSolver[WRITE]   <= 1 ||
	 fromSolver[READ]  <= 1 || 
	 fromSolver[WRITE] <= 1 ) die("Failed to setup pipes");

    // Setup signal handler
    signal(SIGPIPE, PipeSolver::signalHandler);

    // Fork into a solver process and a read/write process
    solverpid = vfork();
    if (solverpid == 0) {
        // Child ("solver") process
        // Set up pipes to stdin and stdout
        close(toSolver[WRITE]);
        close(fromSolver[READ]);
        dup2 (toSolver[READ], STDIN_FILENO);
        dup2 (fromSolver[WRITE], STDOUT_FILENO);
        close(fromSolver[WRITE]);
        close(toSolver[READ]);

        // Redirect solver's stderr to NULL
        dup2 (0, STDERR_FILENO);
      
        // Execute solver
	execv(args[0], args);
        raise(SIGPIPE);
	exit(1);
    }
    // Exit on fork error
    else if ( solverpid < 0 ) die("Failed to fork");

    // Close unnecessary pipes
    close(toSolver[READ]);
    close(fromSolver[WRITE]);
    
    // Obtain file pointer to stream
    out = fdopen (toSolver[WRITE], "w");
    in  = fdopen (fromSolver[READ], "r");
    assert(in);
    assert(out);

    if ( !in || !out )
      die("Failed to obtain file pointer to pipe\n");

    if ( pthread_create (&rthread, NULL, PipeSolver::readThreadEntry, (void*) this) )
      die("Failed to create new POSIX thread");

    // Print problem header to solver
    fprintf(out, "p inccnf\n");
}

// Destructor
PipeSolver::~PipeSolver() {
    // Close pipe from this program to the solver
    if ( out ) fclose(out);
    // Wait for solver child process
    if ( solverpid > 0 ) waitpid(solverpid, NULL, 0);
    if ( in ) {
        // Join read thread
        pthread_join(rthread, NULL);
        // Close pipe from the solver to this program
        fclose(in);
    }
}

void PipeSolver::signalHandler(int signum) {
    assert( signum == SIGPIPE );    
    die("Broken pipe (failed to launch solver?)");
}

// Add a clause to the solver by printing it into the pipe
void PipeSolver::addClause(const int* clause) {
    printConstraint(out, clause);
}

// Add a unit clause to the solver by printing it into the pipe
void PipeSolver::addUnitClause(int lit) { 
    fprintf(out, "%d 0\n", lit);
}

// Add a cube to the solver, this will create a job
int PipeSolver::addCube(const int* cube) {
    fprintf(out, "a ");
    printConstraint(out, cube);    
    fflush(out);
    return numCubes++;
}

// Add negation of a clause as a cube to the solver, this will create a job
int PipeSolver::addNegationAsCube(const int* clause) {
    assert( clause );
    fprintf(out, "a ");
    while( *clause ) fprintf(out, "%d ", -(*(clause++)));
    fprintf(out, "0\n");
    fflush(out);
    return numCubes++;
}

// Parses the output from the solver and calls the user specified report function
void PipeSolver::readThread() {
    // If there is no solver output at all then there's nothing to do
    if ( feof(in) ) return;

    int  job;
    int  cap    = 0;
    int* array  = NULL;
    bool parsed = false;

    // Each line should start with 'Job %d', 
    // followed by spaces, 
    // followed by 'SAT' or 'UNSAT', 
    // followed by a sequence of integers terminated by the integer 0
    // after the '0' integer and before the newline character any character may appear
    while( fscanf(in, "Job %d", &job) == 1 ) {
        parsed = true;

        int c;
        // Skip whitespace
        do c = fgetc(in); while( c == ' ' );

        // Matches either SAT or UNSAT
	int result = c;
        if (c == 'U' && fgetc(in) == 'N' ) c = fgetc(in);
	if (c != 'S' || fgetc(in) != 'A' || fgetc(in) != 'T' )
	    die("Solver result parse error, expected SAT or UNSAT after 'Job %d'", job);

	// Read assignment/conflict
	int v;
	int i = 0;
	do {
	    // Read integer
  	    if ( fscanf(in, "%d", &v) != 1 )
	        die("Solver result parse error, expected integer in %s for job %d\n", 
                    (result=='S') ? "assignment":"conflict", job);
	  
  	    // Increase size of array if necessary
 	    if ( i == cap ) {
	        cap+= ALLOC;
	        array = (int*) realloc(array, sizeof(*array) * cap);
	    }
  	    assert( i < cap );

  	    // Push element
	    array[i++] = v;
	}
	while( v );

	// Call user specified function to report the result
	reportResult(data, job, (result == 'S') ? SATISFIABLE : UNSATISFIABLE, array);

	// Skip to end of line
        do c = fgetc(in); while( c != EOF && c != '\n' );
    }

    // Free any memory allocated for the array
    if ( array ) free(array);

    // If we didn't manage to parse anything report error
    if ( !parsed ) die("Solver output contained no parsable results");
}
