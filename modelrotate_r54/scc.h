#ifndef scc_h
#define scc_h
#include"mtl/Vec.h"
#include"core/SolverTypes.h"

using namespace Minisat;

class MClause;
class SCC : public vec<MClause*> {
 public:
    SCC() : redundant(false), critical(false), inSolver(false), id(scc_ids++) {}

    inline const int        getId          () const { return id; }
    inline const vec<SCC*>& getEdges       () const { return edges; }
    inline const bool       isReachable    () const { return reachableFrom.size()>0; }
    inline const bool       isCritical     () const { return critical; }
    inline const bool       isRedundant    () const { return redundant; }
    inline const bool       isInSolver     () const { return inSolver; }

    void cleanReachableFromList ();

    int setCritical ();  
    inline void setRedundant() { assert(!critical); redundant = true; }
    inline void setInSolver () { inSolver = true; }
    
    void addEdge(SCC *c);
 private:
    pthread_mutex_t lock;
    
    vec<SCC*>   edges;
    vec<SCC*>   reachableFrom;
    bool        redundant;
    bool        critical;
    bool        inSolver;
    const  int  id;
    static int  scc_ids;
};

#endif
