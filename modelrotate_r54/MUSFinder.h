//
// Written by Siert Wieringa - siert.wieringa@aalto.fi
// (c) Aalto University School of Science 2012
//
//
#ifndef musfinder_h
#define musfinder_h
#include "mtl/Vec.h"
#include "core/Solver.h"
#include "utils/Options.h"
#include "MClause.h"
#include <pthread.h>

using namespace Minisat;

class MUSFinder {
public:
     MUSFinder();
    ~MUSFinder();

    int  go         ();
    bool addClause  (vec<Lit> &ps);
    void printStats ();

    // MiniSAT style interface (allows use of MiniSAT's parser)
    inline bool addClause_ (vec<Lit>& ps) { return addClause(ps); }
    inline int  newVar     ()             { return varCnt++; }
    inline int  nVars      () const       { return varCnt; }

    static IntOption  verbosity;
    static IntOption  pickSCC;
    static IntOption  rotation;
    static IntOption  cores;
    static BoolOption reduce;
    static BoolOption calcSCC;
    static BoolOption stats;

    void handlePipeSolverResult(int jobId, bool result, const int *modelOrConflict);
private:
    SCC& chooseSCC();
    bool isAssoc(const vec<lbool> &a, const MClause &c);
    int  createAssoc(MClause& src, MClause& dest, Lit lit);
    void createEdges(MClause& c, const Lit& l);
    void createEdges(MClause& c);
    void initialize();
    void reduceMinisat();
    void reducePipeSolver(char *const args[]);

    void handleSAT  (MClause *c, vec<lbool>& model);
    void handleUNSAT(vec<Lit>& conflict);

    vec<MClause*> clauses;
    vec<vec<MClause*> > occ;

    vec<MClause*> jobList;
    vec<SCC*> sccs;
    vec<Lit>  unitClauses;
    int satCnt;
    int unsatCnt;
    int effSatCnt;
    int effUnsatCnt;
    int varCnt;
    int maxJobs;
    int pendingJobs;
    int criticalClauses;
    int clauseCnt;

    pthread_mutex_t lock;
    pthread_cond_t newjobcond;
};

#endif
