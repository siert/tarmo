#!/bin/bash
function exiterror {
    echo -e "[$0] ERROR: " $@ 1>&2
    exit 1
}


if [ ! -e "$1" ]; then
    exiterror "File '"$1"' does not exist"
elif [ "$SBIN" == "" ]; then
    exiterror "Environment variable 'SBIN' (solver binary) not set"
elif [ "$T" == "" ]; then
    exiterror "Environment variable 'T' (number of threads in srun) not set"
elif [ "$R" == "" ]; then
    exiterror "Environment variable 'R' (number of threads in sbatch reservation) not set"
elif [ "$TIME_FOR_SBATCH" == "" ]; then
    exiterror "Environment variable 'TIME_FOR_SBATCH' not set"
elif [ "$TIME_FOR_SRUN" == "" ]; then
    exiterror "Environment variable 'TIME_FOR_SRUN' not set"
elif [ "$MEM" == "" ]; then
    exiterror "Environment variable 'MEM' not set"
elif [ "$PARTITION" == "" ]; then
    exiterror "Environment variable 'PARTITION' not set"
fi

#Optional: E, RUN, P

FILE=$1
LOG=log/$FILE
mkdir -p $LOG
if [ ! -d $LOG ]; then
    exiterror "Failed to create directory '"$LOG"'"
fi

LOCALDIR=/state/partition1/swiering
USRBINTIME=/usr/bin/time

S=$(echo $P | gawk '{ gsub(" ", "_"); print }' )
LOGNAME=${LOG}/$(basename $SBIN)$S$RUN

if [ "$(ls ${LOGNAME}.err 2>/dev/null)" != "" ]; then
    exiterror "Result '"${LOGNAME}"*.err' already exists"
fi

echo "#!/bin/bash"
echo "#SBATCH ${TIME_FOR_SBATCH} ${MEM}"
echo "#SBATCH --output=/dev/null --error=/dev/null"
echo "#SBATCH --job-name=$(basename ${FILE})"
echo "#SBATCH -p ${PARTITION} -n1 -c$R --constraint=xeon --qos=short "$E
echo "  TMPDIR=${LOCALDIR}/\${SLURM_JOBID}"
echo "  mkdir -p \${TMPDIR}"
echo "  MOUT=\$(mktemp)"
echo "  MERR=\$(mktemp)"
echo
echo "  trap \"mv \${MOUT} ${LOGNAME}.out; mv \${MERR} ${LOGNAME}.err; rm -rf \${TMPDIR}\" TERM EXIT"
echo
echo "  srun -c$T ${TIME_FOR_SRUN} --error=\${MERR} --output=\${MOUT} ${USRBINTIME} ${SBIN} ${FILE} $P &"
echo "  wait"
