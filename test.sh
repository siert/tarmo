#/bin/bash

if [ ! -e "$1" ]; then
    echo "ERROR: Specify existing file" 1>&2
    exit 1
fi

SBATCH="sbatch"   

export PARTITION="short"
export TIME_FOR_SBATCH="--time=00:40:00"
export TIME_FOR_SRUN="--time=00:30:00"
export MEM="--mem=7500MB"

if [ "$2" == "PLAY" ]; then
    export PARTITION="play"
    export TIME_FOR_SBATCH="--time=00:07:00"
    export TIME_FOR_SRUN="--time=00:05:00"
    export MEM="--mem=1500MB"
elif [ "$2" == "SHOW" ]; then
    SBATCH="cat"
elif [ "$2" != "REALLY" ]; then
    echo "ERROR: Second argument must be one of PLAY, SHOW or REALLY" 1>&2
    exit 1
fi

export RUN=""
export T=2
export R=4
export E=""
export SBIN="./glucored_release-private-r27"

for NP in "-removesubsumed -no-removesubsumed"; do
    export P=$NP
    if [ $(basename $1 .bz2) == $(basename $1) ]; then
	./triton.sh $1 | $SBATCH
    else
	./triton_bz2.sh $1 | $SBATCH
    fi
done
