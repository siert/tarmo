#!/bin/sh
# File created: 2009-06-30 16:00:31
if [ -z "$1" ]; then
  echo "Usage: $0 <log file>" >&2
  exit 1
fi
echo $1:
x=`grep -F elapsed $1 | head -n 1 | cut '-d '  -f1,3 | tr -d a-z`

cpu=`echo "$x" | cut '-d ' -f1`
real=`echo "$x" | cut '-d ' -f2`

if [ -z `echo $real | cut -d: -f3` ]; then
  hours=0
  minutes=`echo $real | cut -d: -f1`
  seconds=`echo $real | cut -d: -f2`
else
  hours=`echo $real | cut -d: -f1`
  minutes=`echo $real | cut -d: -f2`
  seconds=`echo $real | cut -d: -f3`
fi

real=`echo "$hours*60*60 + $minutes*60 + $seconds" | bc`

len1=`echo $cpu | cut -d. -f1 | wc -c`
len2=`echo $real | cut -d. -f1 | wc -c`

lenmax=`/bin/echo -e "$len1\n$len2" | sort -n | tail -n1`
lenmax=`echo $lenmax+3 | bc`

/bin/echo -ne "\tCPU: "
printf "%${lenmax}.2f\n" $cpu
/bin/echo -ne "\tReal:"
printf "%${lenmax}.2f\n" $real
