#/bin/bash

# Pipe the solver output followed by the CNF file through here
gawk '{
    if ( $1 == "s" ) {
        lits=0;
        clauses=0;
        if ( $2 != "SATISFIABLE" || NF != 2 ) {
            print "ERROR expected \"s SATISFIABLE\"";
            exit 1;
        }
    }
    else if ( $1 == "v" ) {
        if ( $NF != 0 ) {
            print "ERROR expected zero at end of satisfying assignment"; 
            exit 1;
        }

        for( i = 2; i < NF; i++ ) { 
            v=$i; 
            if ( v == 0 ) { 
                print "ERROR unexpected zero in satisfying assignment"; 
                exit 1; 
            }
            else { 
                t[v]=1; t[-v]=-1; 
            } 
        }
    }
    else if ( $1 != "c" && $1 != "p" ) {
        for( i = 1; i <= NF; i++ ) {
            v=$i;
            if ( v == 0 ) {
                if ( lits == 0 ) { print "ERROR unsatisfied clause: "; print; exit 1; } 
                lits = 0;
                clauses++;
            }
            else if ( t[v] == 1 ) lits++;
            else if ( t[v] != -1 ) { print "ERROR variable "v" has no value"; exit 1; }
        }
    }
} END {
    print "Verified "clauses" satisfied clauses"
}'
